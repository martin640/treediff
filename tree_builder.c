#include "tree_builder.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>

DIR *opendir_handle_error(char *path) {
    DIR *d = opendir(path);
    if (!d) {
        fprintf(stderr, "Failed to open directory: %s\n", path);
        exit(EXIT_FAILURE);
    }
    return d;
}

char *join_path(char *base, char *name) {
    char out[FILENAME_MAX] = "";
    strcat(out, base);
    strcat(out, kPathSeparator);
    strcat(out, name);
    char *trimmed = malloc(strlen(out) + 1);
    strcpy(trimmed, out);
    return trimmed;
}

void parse_dir_children(struct TreeNode *node) {
    struct dirent *child;
    struct stat *statbuf = malloc(sizeof(struct stat));
    DIR *d = opendir_handle_error(node->path);
    unsigned int index = 0, arr_size = TREE_CHILDREN_BUFFER;
    struct TreeNode **files = malloc(arr_size * sizeof(struct TreeNode*));
    for (int i = 0; i < arr_size; i++) {
        files[i] = malloc(sizeof(struct TreeNode));
    }
    while ((child = readdir(d)) != NULL) {
        // we are not interested in current directory and parent
        if (strcmp(child->d_name, ".") == 0 || strcmp(child->d_name, "..") == 0) continue;
        // check if files list has exceeded its capacity and enlarge it if needed
        if (index >= arr_size) {
            unsigned int new_size = arr_size + TREE_CHILDREN_BUFFER;
            struct TreeNode **files_new = malloc(new_size * sizeof(struct TreeNode*));
            for (int i = 0; i < new_size; i++) {
                if (i < arr_size) files_new[i] = files[i];
                else files_new[i] = malloc(sizeof(struct TreeNode));
            }
            free(files);
            files = files_new;
            arr_size = new_size;
        }
        files[index]->name = strdup(child->d_name);
        files[index]->path = join_path(node->path, child->d_name);
        if (stat(files[index]->path, statbuf) != 0) {
            fprintf(stderr, "Failed to stat directory: %s\n", files[index]->path);
            exit(EXIT_FAILURE);
        }
        files[index]->directory = S_ISDIR(statbuf->st_mode);
        files[index]->differs = false;
        files[index]->size = statbuf->st_size;
        files[index]->child_count = 0;
        files[index]->parent = node;
        files[index]->sibling = NULL;
        index++;
    }
    closedir(d);
    node->child_count = index;
    node->children = files;
}

void parse_subtree(struct TreeNode *node1, struct TreeNode *node2, unsigned int level) {
    parse_dir_children(node1);
    parse_dir_children(node2);
    
    struct TreeNode *c1, *c2;
    for (int i1 = 0; i1 < node1->child_count; i1++) {
        c1 = node1->children[i1];
    
        for (int i2 = 0; i2 < node2->child_count; i2++) {
            c2 = node2->children[i2];
            if (strcmp(c1->name, c2->name) == 0) {
                c1->sibling = c2;
                c2->sibling = c1;
                if (c1->directory) {
                    parse_subtree(c1, c2, level + 1);
                }
                goto skip;
            }
        }
        // no match found on right side, propagate difference to the tree root
        c1->differs = true;
        struct TreeNode *parent = c1;
        while ((parent = parent->parent) != NULL) {
            parent->differs = true;
        }
        skip:;
    }
    for (int i1 = 0; i1 < node2->child_count; i1++) {
        c1 = node2->children[i1];
        if (c1->sibling != NULL) continue;
        c1->differs = true;
        struct TreeNode *parent = c1;
        while ((parent = parent->parent) != NULL) {
            parent->differs = true;
        }
    }
}

struct TreeNode *build_tree(char *left, char *right) {
    struct stat *statbuf1 = malloc(sizeof(struct stat));
    struct stat *statbuf2 = malloc(sizeof(struct stat));
    if (stat(left, statbuf1) != 0) {
        fprintf(stderr, "Failed to stat first directory: %s\n", left);
        exit(EXIT_FAILURE);
    }
    if (stat(right, statbuf2) != 0) {
        fprintf(stderr, "Failed to stat second directory: %s\n", right);
        exit(EXIT_FAILURE);
    }
    if (!S_ISDIR(statbuf1->st_mode)) {
        fprintf(stderr, "First path is not directory: %s\n", left);
        exit(EXIT_FAILURE);
    }
    if (!S_ISDIR(statbuf2->st_mode)) {
        fprintf(stderr, "Second path is not directory: %s\n", right);
        exit(EXIT_FAILURE);
    }
    struct TreeNode *leftNode = malloc(sizeof(struct TreeNode));
    leftNode->name = strdup(left);
    leftNode->path = strdup(left);
    leftNode->directory = true;
    leftNode->differs = false;
    leftNode->size = statbuf1->st_size;
    leftNode->child_count = 0;
    leftNode->parent = NULL;
    leftNode->sibling = NULL;
    
    struct TreeNode *rightNode = malloc(sizeof(struct TreeNode));
    rightNode->name = strdup(right);
    rightNode->path = strdup(right);
    rightNode->directory = true;
    rightNode->differs = false;
    rightNode->size = statbuf2->st_size;
    rightNode->child_count = 0;
    rightNode->parent = NULL;
    rightNode->sibling = leftNode;
    leftNode->sibling = rightNode;
    parse_subtree(leftNode, rightNode, 0);
    return leftNode;
}

void free_tree_internal(struct TreeNode *node, bool free_sibling) {
    free(node->name);
    free(node->path);
    if (free_sibling && node->sibling != NULL) free_tree_internal(node->sibling, false);
    if (node->directory) {
        for (int i = 0; i < node->child_count; i++) {
            struct TreeNode *c = node->children[i];
            free_tree_internal(c, false);
        }
    }
    free(node);
}

void free_tree(struct TreeNode *node) {
    free_tree_internal(node, true);
}