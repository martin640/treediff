#ifndef TREEDIFF_TREE_BUILDER_H
#define TREEDIFF_TREE_BUILDER_H

#include <stdbool.h>
#include <dirent.h>

static const unsigned int TREE_CHILDREN_BUFFER = 128;

static const char *kPathSeparator =
#ifdef _WIN32
    "\\\0";
#else
    "/\0";
#endif

struct TreeNode {
    char *name;
    char *path;
    bool directory;
    bool differs;
    unsigned long size;
    unsigned int child_count;
    struct TreeNode *parent;
    struct TreeNode *sibling;
    struct TreeNode **children;
};

struct TreeNode *build_tree(char *left, char *right);

void free_tree(struct TreeNode *node);

#endif //TREEDIFF_TREE_BUILDER_H
