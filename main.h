#ifndef TREEDIFF_MAIN_H
#define TREEDIFF_MAIN_H

#include "tree_builder.h"
#include <dirent.h>
#include <stdbool.h>

int main(int argc, char *argv[]);

void iterateNode(struct TreeNode *node, int level);

void print_diff(struct TreeNode *left, struct TreeNode *right, int level);

void print_row(char *left, char *right, char *sep, char *color, int offset);

#endif //TREEDIFF_MAIN_H
