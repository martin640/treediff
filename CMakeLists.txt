cmake_minimum_required(VERSION 3.22)
project(treediff C)

set(CMAKE_C_STANDARD 23)

add_executable(treediff main.c main.h tree_builder.c tree_builder.h)
