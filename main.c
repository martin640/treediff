#include "main.h"
#include "tree_builder.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>

#define RED   "\x1B[31m"
#define YEL   "\x1B[33m"
#define BLU   "\x1B[34m"
#define RESET "\x1B[0m"

static const char *usage = "Usage: %s [-dc] folder1 folder2 [folders...]\n";
static const int column_width = 56;
static const int indent = 2;
static bool diff_only = false;
static bool use_color = false;

int main(int argc, char *argv[]) {
    int opt;
    while ((opt = getopt(argc, argv, "dc")) != -1) {
        switch (opt) {
            case 'd':
                diff_only = true;
                break;
            case 'c':
                use_color = true;
                break;
            default:
                fprintf(stderr, usage, argv[0]);
                exit(EXIT_FAILURE);
        }
    }
    if (optind+1 >= argc) {
        fprintf(stderr, usage, argv[0]);
        exit(EXIT_FAILURE);
    }
    char *dir_path_left = argv[optind];
    char *dir_path_right = argv[optind + 1];
    
    struct TreeNode *tree = build_tree(dir_path_left, dir_path_right);
    iterateNode(tree, 0);
    free_tree(tree);
}

void iterateNode(struct TreeNode *node, int level) {
    print_diff(node, node->sibling, level);
    
    for (int i = 0; i < node->child_count; i++) {
        struct TreeNode *c = node->children[i];
        if (c->directory)
            iterateNode(c, level + 1);
        else
            print_diff(c, c->sibling, level);
    }
    if (node->sibling != NULL && node->sibling->directory) {
        for (int i = 0; i < node->sibling->child_count; i++) {
            struct TreeNode *c = node->sibling->children[i];
            if (c->sibling == NULL)
                print_diff(c->sibling, c, level);
        }
    }
}

void print_diff(struct TreeNode *left, struct TreeNode *right, int level) {
    if (diff_only) {
        if (left == NULL) {
            printf("[  Left missing ] %s\n", right->name);
        } else if (right == NULL) {
            printf("[ Right missing ] %s\n", left->name);
        } else if (left->size != right->size) {
            char x = (left->size > right->size) ? '>' : '<';
            printf("[ Size mismatch ] %s (%lu %c %lu)\n", left->name, left->size, x, right->size);
        }
    } else {
        if (left == NULL) {
            print_row("", right->name, "X", RED, level * indent);
        } else if (right == NULL) {
            print_row(left->name, "", "X", RED, level * indent);
        } else if (left->size != right->size) {
            char x = (left->size > right->size) ? '>' : '<';
            print_row(left->name, right->name, &x, YEL, level * indent);
        } else {
            print_row(left->name, right->name, "|", RESET, level * indent);
        }
    }
    
}

void print_row(char *left, char *right, char *sep, char *color, int offset) {
    size_t needed = snprintf(NULL, 0, "%%%ds%%-%ds | %%%ds\n", offset, column_width - offset, offset) + 1;
    char *format = malloc(needed);
    sprintf(format, "%%s%%%ds%%-%ds %s %%%ds%%s%%s\n", offset, column_width - offset, sep, offset);
    printf(format, use_color ? color : "", "", left, "", right, use_color ? RESET : "");
    free(format);
}




