CC=gcc
CFLAGS= -std=c2x -pedantic -Wall -Wextra -g
IDIR=.
ODIR=obj

_DEPS = main.h tree_builder.h
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

_OBJ = main.o tree_builder.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

treediff: $(OBJ)
	$(CC) $(CFLAGS) -o $@ $^

$(ODIR)/%.o: %.c $(DEPS)
	$(CC) $(CFLAGS) -c -o $@ $<

