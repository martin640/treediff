# treediff

Warning: This project is just an experiment and not meant to be used in a production.

## Usage

`./treediff [-dc] folder1 folder2`

`-d` - print differences only

`-c` - use colors (red for missing file, yellow for sizes mismatch); not implemented with `-d` option

When printing trees, `folder1` (left tree) is fully traversed even if right tree doesn't have matching entry. This doesn't apply for right tree and when an entry (let it be folder *y*) doesn't exist on left side, recursive traversal of folder *y* is skipped.
